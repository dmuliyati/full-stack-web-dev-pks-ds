<?php
//Parent Class
abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $darah, $jumlahKaki, $keahlian){
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    public function atraksi(){
        echo $this->nama . " sedang " . $this->keahlian . "<br>";
    }
}

abstract class Fight extends Hewan{
    public $attackPower;
    public $defencePower;
    public $musuh;

    public function setattackPower($attackPower){
        $this->attackPower = $attackPower;
    }
    public function getattackPower(){
        return $this->attackPower;
    }

    public function setdefencePower($defencePower){
        $this->defencePower = $defencePower;
    }

    public function getdefencePower(){
        return $this->defencePower;
    }

    abstract public function serang();
    abstract public function diserang();

}

class Elang extends Fight{
    public $musuh = "Harimau";
    public function serang() : string {
        return $this->nama . " sedang menyerang ".$this->musuh . "<br>";
      }
    public function diserang() : string {
        return $this->nama . " sedang diserang ".$this->musuh . "<br>";
    }
    public function getInfo(){
        echo "Nama Hewan : " . $this->nama . "<br>";
        echo "Jumlah Darah: " . $this->darah . "<br>";
        echo "Jumlah Kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian : " . $this->keahlian . "<br>";
        echo "Attack Power: " . $this->attackPower . "<br>";
        echo "Defence Power" . $this->defencePower . "<br><br>";
    }

}

class Harimau extends Fight{
    public $musuh = "Elang";
    public function serang() : string {
        return $this->nama . " sedang menyerang ".$this->musuh . "<br>";
      }
    public function diserang() : string {
        return $this->nama . " sedang diserang ".$this->musuh . "<br>";
    }
    public function getInfo(){
        echo "Nama Hewan : " . $this->nama . "<br>";
        echo "Jumlah Darah: " . $this->darah . "<br>";
        echo "Jumlah Kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian : " . $this->keahlian . "<br>";
        echo "Attack Power: " . $this->attackPower . "<br>";
        echo "Defence Power" . $this->defencePower . "<br><br>";
    }

}

$elang = new Elang("Elang", 50, 2, "terbang");
$elang->setattackPower(10);
$elang->setdefencePower(5);

$harimau = new Harimau("Harimau", 50, 4, "lari cepat");
$harimau->setattackPower(7);
$harimau->setdefencePower(8);

echo $elang->atraksi();
echo $harimau->atraksi();
echo "<br>";
echo $elang->getInfo();
echo $harimau->getInfo();


echo $elang->serang();
echo $elang->diserang();
?>