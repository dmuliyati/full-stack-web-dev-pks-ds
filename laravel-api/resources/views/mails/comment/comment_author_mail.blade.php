<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    Saudara/i {{ $comment->user->name }} , komentar Anda yang berisi : " {{ $comment->content }}", yang ditujukan kepada pemilik artikel {{ $comment->post->user->name }}, telah berhasil disubmit.
</body>
</html>
