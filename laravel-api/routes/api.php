<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('/test', function(){
    return 'Status Masuk: Selamat Datang di Aplikasi Web Service dmuliyati';
})->middleware('auth:api');
/*
Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@store');
Route::get('/post/{id}', 'PostController@show');
*/
Route::apiResource('post', 'PostController');
Route::apiResource('comment', 'CommentController');
Route::apiResource('role', 'RoleController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
    ], 
    function(){
        Route::post('register','RegisterController')->name('auth.register');
        Route::post('regenerate-otp-code','RegenerateOtpCodeController')->name('auth.regenerate-otp-code');
        Route::post('verification','VerificationController')->name('auth.verification');
        Route::post('update-password','UpdatePasswordController')->name('auth.update-password');
        Route::post('login','LoginController')->name('auth.login');
});
