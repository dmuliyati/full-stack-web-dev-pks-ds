<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Events\CommentStoredEvent;
//use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
//use App\Mail\CommentAuthorMail;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;
        $comments = Comment::where('post_id',$post_id)->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Komen berhasil ditampilkan',
            'data' => $comments
        ], 200);
    }
    public function show($id)
    {
        $comment = Comment::find($id);

        if ($comment){
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Komentar',
                'data'    => $comment
            ], 200);            
        }
        return response()->json([
            'success' => false,
            'message' => 'Komen dengan id : '.$id.'tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allrequest = $request->all();
        $validator = Validator::make($allrequest,[
            'content' => 'required',
            'post_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = auth()->user();
        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
            'user_id' => $user->id
        ]);

        //Memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comment));



        //dikirim ke user pemilik post
        //Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        //dikirim ke user penulis comment
        //Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Data komentar berhasil dibuat',
                'data' => $comment,
            ], 201);
        }
        return response()->json([
            'success' => false,
            'message' => 'Komen gagal dibuat',
        ], 409);
    }

    public function update(Request $request, $id)
    {       
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
    
        //find comment by ID
        $comment = Comment::find($id);
        
        $user = auth()->user();
        if($comment->user_id != $user->id){
            return response()->json([
                'success' => false,
                'message' => 'Hak Akses ditolak'], 403);
        }

        if($comment) {

            //update comment
            $comment->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Komentar Diperbarui',
                'data'    => $comment 
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Komen dengan id : '.$id.'tidak ditemukan',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        $user = auth()->user();
        if($comment->user_id != $user->id){
            return response()->json([
                'success' => false,
                'message' => 'Hak Akses ditolak'], 403);
        }

        if($comment) {

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Komen berhasil Dihapus',
            ], 200);

        }

        return response()->json([
            'success' => false,
            'message' => 'Komen dengan id : '.$id.'tidak ditemukan',
        ], 404);
    }
}
