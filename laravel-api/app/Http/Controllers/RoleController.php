<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Data daftar role berhasil ditampilkan',
            'data' => $roles
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allrequest = $request->all();
        $validator = Validator::make($allrequest,[
            'name' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $role = Role::create([
            'name' => $request->name,
        ]);
        if($role){
            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil dibuat',
                'data' => $role,
            ], 201);
        } 
        
        return response()->json([
            'success' => false,
            'message' => 'Role gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //find post by ID
        $role = Role::find($id);
        if($role){
        //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Role',
                'data'    => $role
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Role dengan id : '.$id.'tidak ditemukan',
        ], 409);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $role = Role::find($id);
        if($role) {

            //update post
            $role->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Diperbarui',
                'data'    => $role  
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Tidak Ditemukan',
        ], 404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        if($role) {

            //delete post
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Dihapus',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Tidak Ditemukan',
        ], 404);
    }
}
