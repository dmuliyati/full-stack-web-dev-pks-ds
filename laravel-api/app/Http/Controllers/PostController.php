<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }

    public function index()
    {
        $posts = Post::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan',
            'data' => $posts
        ], 200);
    }
    public function show($id)
    {
        //find post by ID
        $post = Post::findOrfail($id);
        if($post){
        //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Post',
                'data'    => $post 
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Post dengan id : '.$id.'tidak ditemukan',
        ], 409);

    }
    public function store(Request $request)
    {
        $allrequest = $request->all();
        $validator = Validator::make($allrequest,[
            'title' => 'required',
            'description' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $user->id
        ]);
        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dibuat',
                'data' => $post,
            ], 201);
        } 
        
        return response()->json([
            'success' => false,
            'message' => 'Post gagal dibuat',
        ], 409);
    }
    public function update(Request $request, Post $post)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = Post::findOrFail($post->id);
        

        if($post) {
            $user = auth()->user();
            if($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Hak Akses ditolak'], 403);
            }

            //update post
            $post->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Diperbarui',
                'data'    => $post  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Tidak Ditemukan',
        ], 404);

    }
    
    /** 
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        
        //find post by ID
        $post = Post::findOrfail($id);
        $user = auth()->user();
        if($post->user_id != $user->id){
            return response()->json([
                'success' => false,
                'message' => 'Hak Akses ditolak'], 403);
        }

        if($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Dihapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Tidak Ditemukan',
        ], 404);
    }

}