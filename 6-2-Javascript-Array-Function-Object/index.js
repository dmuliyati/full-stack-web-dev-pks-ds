console.log("\nSoal 1")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var orderedDH = daftarHewan.slice().sort()
for (var i in daftarHewan){
    console.log(orderedDH[i])
}

console.log("\nSoal 2")
var detail = {name : "name" , age : 20 , address : "address" , hobby : "hobby" }
function introduce(detail) {
    return console.log("Nama saya "+detail.name+", umur saya "+detail.age+" tahun, alamat saya di "+detail.address+", dan saya punya hobby yaitu "+detail.hobby)
  }
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)

console.log("\nSoal 3")
function hitung_huruf_vokal(str) {
    var m = str.match(/[aeiou]/gi)
    return m === null ? 0 : m.length
  }
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2)

console.log("\nSoal 4")
function hitung(angka = 0){
    return angka*2-2
}
console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) )