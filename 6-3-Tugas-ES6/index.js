console.log("\nSoal 1")
const persegipanjang = (panjang, lebar) => {
    let luas = panjang*lebar
    let keliling = 2*panjang + 2*lebar
    console.log(`Luas: ${luas}\nKeliling: ${keliling}`)
};
persegipanjang(4,5);

console.log("\n\nSoal 2")
const newFunction = (firstName, lastName) => {
    return {
      fullName(){
        console.log(firstName + " " + lastName)
      }
    }
  };
newFunction("William", "Imoh").fullName() ;

console.log("\n\nSoal 3")
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };
const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

console.log("\n\nSoal 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]
console.log(combined)

console.log("\n\nSoal 5")
const planet = "earth" 
const view = "glass" 
let after = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`;
console.log(after)